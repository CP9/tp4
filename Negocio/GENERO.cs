﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace Negocio
{
    public class GENERO
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public override string ToString()
        {
            return nombre.ToUpper();
        }


        public static List<GENERO> ListarTodos()
        {
            List<GENERO> lista = new List<GENERO>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("Genero_Listar");
            acceso.Cerrar();


            tabla.Columns[1].ColumnName = "Género";

            foreach(DataRow registro in tabla.Rows)
            {
                GENERO genero = new GENERO();
                genero.id = int.Parse( registro["ID_GENERO"].ToString()  );
                genero.nombre = registro["Género"].ToString();

                lista.Add(genero);
            }
            return lista;
        }



    }
}