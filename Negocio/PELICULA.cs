﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace Negocio
{
    public class PELICULA
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private GENERO genero;

        public GENERO Genero        
        {
            get { return genero; }
            set { genero = value; }
        }



        public static List<PELICULA> listar()
        {
            List<PELICULA> lista = new List<PELICULA>();

            List<GENERO> generos = GENERO.ListarTodos();


            Acceso acceso = new Acceso();
            acceso.Abrir();

            DataTable tabla = acceso.Leer("PELICULA_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                PELICULA pel = new PELICULA();
                pel.id = int.Parse(registro["ID_PELICULA"].ToString());
                pel.Nombre = registro["NOMBRE"].ToString();

                pel.genero = (from GENERO g in generos
                              where g.Id == int.Parse(registro["ID_GENERO"].ToString())
                              select g).FirstOrDefault();
                

                lista.Add(pel);
            }


            return lista;
        }



    }
}