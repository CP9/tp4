﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace PresentacionCine
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            EnlazarGeneros();
            Enlazar();
        }
        private void Enlazar()
        {
            GRILLA.DataSource = null;
            GRILLA.DataSource = PELICULA.listar();
        }
        private void EnlazarGeneros()
        {
            comboBox1.DataSource = null;
            comboBox1.DataSource = Negocio.GENERO.ListarTodos();

        }

    }
}
