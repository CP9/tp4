USE [CINE]
GO
/****** Object:  Table [dbo].[ESTADO]    Script Date: 2/9/2020 21:52:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ESTADO](
	[ID] [int] NOT NULL,
	[ESTADO] [varchar](50) NULL,
 CONSTRAINT [PK_ESTADO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GENERO]    Script Date: 2/9/2020 21:52:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GENERO](
	[ID_GENERO] [int] NOT NULL,
	[GENERO] [varchar](50) NOT NULL,
 CONSTRAINT [PK_GENERO] PRIMARY KEY CLUSTERED 
(
	[ID_GENERO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PELICULA]    Script Date: 2/9/2020 21:52:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PELICULA](
	[ID_PELICULA] [int] NOT NULL,
	[NOMBRE] [varchar](50) NOT NULL,
	[ID_GENERO] [int] NOT NULL,
	[ID_ESTADO] [int] NULL,
 CONSTRAINT [PK_PELICULA] PRIMARY KEY CLUSTERED 
(
	[ID_PELICULA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PELICULA]  WITH CHECK ADD  CONSTRAINT [FK_PELICULA_GENERO] FOREIGN KEY([ID_GENERO])
REFERENCES [dbo].[GENERO] ([ID_GENERO])
GO
ALTER TABLE [dbo].[PELICULA] CHECK CONSTRAINT [FK_PELICULA_GENERO]
GO
/****** Object:  StoredProcedure [dbo].[GENERO_Borrar]    Script Date: 2/9/2020 21:52:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[GENERO_Borrar]
@id int as
BEGIN

if not exists(select * from PELICULA where ID_GENERO = @id)
begin
	delete from genero
	where ID_GENERO = @id
end
END


GO
/****** Object:  StoredProcedure [dbo].[GENERO_EDITAR]    Script Date: 2/9/2020 21:52:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[GENERO_EDITAR] 
@id int, @NOM varchar(50)
as
BEGIN
update genero set
genero = @nom
where ID_GENERO = @id
 
END

GO
/****** Object:  StoredProcedure [dbo].[GENERO_INSERTAR]    Script Date: 2/9/2020 21:52:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GENERO_INSERTAR] 
@NOM varchar(50)
as
BEGIN

Declare @id int
set @id   = isnull((select max(id_genero) from GENERO),0) +1

insert into genero values(@id,@nom)
 
END

GO
/****** Object:  StoredProcedure [dbo].[Genero_Listar]    Script Date: 2/9/2020 21:52:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create proc [dbo].[Genero_Listar]
as 
begin
select * from genero
end

GO
/****** Object:  StoredProcedure [dbo].[PELICULA_Borrar]    Script Date: 2/9/2020 21:52:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[PELICULA_Borrar]
@id int as
BEGIN

	delete from PELICULA
	where ID_PELICULA = @id
END


GO
/****** Object:  StoredProcedure [dbo].[PELICULA_EDITAR]    Script Date: 2/9/2020 21:52:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[PELICULA_EDITAR] 
@id int, @NOM varchar(50),@id_genero int
as
BEGIN
update PELICULA set
NOMBRE= @nom,
ID_GENERO = @id_genero
where ID_PELICULA = @id
 
END

GO
/****** Object:  StoredProcedure [dbo].[PELICULA_INSERTAR]    Script Date: 2/9/2020 21:52:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[PELICULA_INSERTAR] 
@NOM varchar(50), @id_genero int
as
BEGIN

Declare @id int
set @id   = isnull((select max(id_PELICULA) from PELICULA),0) +1

insert into PELICULA values(@id,@nom,@id_genero)
 
END

GO
/****** Object:  StoredProcedure [dbo].[PELICULA_Listar]    Script Date: 2/9/2020 21:52:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create proc [dbo].[PELICULA_Listar]
as 
begin
select * from PELICULA
end

GO
/****** Object:  StoredProcedure [dbo].[PELICULA_Listar_ID]    Script Date: 2/9/2020 21:52:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[PELICULA_Listar_ID]
@id int
as 
begin
select * from PELICULA
where ID_PELICULA = @id
end

GO
USE [master]
GO
ALTER DATABASE [CINE] SET  READ_WRITE 
GO
